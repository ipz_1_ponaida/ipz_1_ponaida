package com.airline.client.context;

import com.airline.client.domain.City;
import com.google.gson.*;
import com.airline.client.domain.Ticket;
import com.airline.client.domain.Way;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class AirlineRequestController implements Callback {

    private static final String URI = "http://localhost:8080";
    private static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private Airline airline;

    public AirlineRequestController() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(LocalDateTime.class, new JsonSerializer<LocalDateTime>() {
                    @Override
                    public JsonElement serialize(LocalDateTime localDateTime, Type type,
                                                 JsonSerializationContext jsonSerializationContext) {
                        return new JsonPrimitive(localDateTime.format(dateTimeFormatter));
                    }
                })
                .registerTypeAdapter(LocalDateTime.class, new JsonDeserializer<LocalDateTime>() {
                    @Override
                    public LocalDateTime deserialize(JsonElement json, Type type,
                                                     JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
                        return LocalDateTime.parse(json.getAsJsonPrimitive().getAsString());
                    }
                })
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URI)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        airline = retrofit.create(Airline.class);
    }

    public List<Way> getWays(Long from, Long to) throws IOException {
        return airline.getWays(from, to).execute().body();
    }

    public Ticket orderTicket(Ticket ticket) throws IOException {
        return airline.orderTicket(ticket).execute().body();
    }

    public List<City> getCities(Long id) throws IOException {
        return airline.getCities(id).execute().body();
    }

    public Ticket getTicket(Long id) throws IOException {
        return airline.getTicket(id).execute().body();
    }

    @Override
    public void onResponse(Call call, Response response) {

    }

    @Override
    public void onFailure(Call call, Throwable t) {

    }
}
