package com.airline.client.context;

import com.airline.client.domain.City;
import com.airline.client.domain.Ticket;
import com.airline.client.domain.Way;
import retrofit2.Call;
import retrofit2.http.*;

import javax.annotation.PostConstruct;
import java.util.List;

public interface Airline {

    @GET("directions")
    Call<List<Way>> getWays(@Query("from") Long from, @Query("to") Long to);

    @GET("cities")
    Call<List<City>> getCities(@Query("id") Long id);

    @POST("tickets")
    Call<Ticket> orderTicket(@Body Ticket ticket);

    @GET("tickets/{id}")
    Call<Ticket> getTicket(@Path("id") Long id);
}
