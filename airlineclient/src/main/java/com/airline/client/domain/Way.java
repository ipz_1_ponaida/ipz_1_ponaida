package com.airline.client.domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Way {

    private Long id;
    private City cityDeparture;
    private City cityArrival;
    private LocalDateTime departure;
    private LocalDateTime arrival;
    private Double price;
}
