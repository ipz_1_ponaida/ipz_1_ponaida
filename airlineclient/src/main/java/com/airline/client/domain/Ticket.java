package com.airline.client.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Ticket {

    private Long id;
    private Way way;
    private String name;
    private String surname;
    private String email;
}
