package com.airline.client.domain;

import lombok.Data;

@Data
public class City {

    private Long id;
    private String name;

    @Override
    public String toString() {
        return name;
    }
}
