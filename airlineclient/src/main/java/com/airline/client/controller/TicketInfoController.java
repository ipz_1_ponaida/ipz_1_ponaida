package com.airline.client.controller;

import com.airline.client.domain.Ticket;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class TicketInfoController {


    public Label idFld;
    public Label customerFld;
    public Label emailFld;
    public Label departingFld;
    public Label arrivalFld;
    public Label priceFld;

    public void initialize() {
        Ticket ticket = AirlineController.ticketToShowInfo;
        idFld.setText("ID: " + ticket.getId());
        customerFld.setText("Customer: " + ticket.getName() + " " + ticket.getSurname());
        emailFld.setText("Email: " + ticket.getEmail());
        departingFld.setText("Departing: " + ticket.getWay().getCityDeparture() + " at: " + ticket.getWay().getDeparture());
        arrivalFld.setText("Arrive: " + ticket.getWay().getCityArrival() + " at: " + ticket.getWay().getArrival());
        priceFld.setText("Price: " + ticket.getWay().getPrice());
    }
}
