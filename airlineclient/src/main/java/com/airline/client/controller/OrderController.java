package com.airline.client.controller;

import com.airline.client.context.AirlineRequestController;
import com.airline.client.domain.Ticket;
import com.airline.client.domain.Way;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.commons.validator.EmailValidator;

import java.io.IOException;
import java.util.*;

public class OrderController {

    public Label priceLbl;
    public TextField nameFld;
    public TextField surnameFld;
    public TextField emailFld;
    public AnchorPane pane;
    public Label fromLbl;
    public Label toLbl;
    public Label departingLbl;
    public Label arriveLbl;

    private AirlineRequestController requestController = new AirlineRequestController();

    private Way way;

    public void initialize() {
        way = AirlineController.wayToOrder;
        fromLbl.setText("From: " + way.getCityDeparture().getName());
        toLbl.setText("To: " + way.getCityArrival().getName());
        departingLbl.setText("Departing time: " + way.getDeparture());
        arriveLbl.setText("Arrival time: " + way.getArrival());
        priceLbl.setText("Price: " + way.getPrice());
    }

    public void createOrder(ActionEvent actionEvent) {
        String name = nameFld.getText();
        String surname = surnameFld.getText();
        String email = emailFld.getText();
        if (!valid(name, surname, email)) {
            showAlert("Incorrect input", "Name and surname length should be more then 3");
        } else if (!isEmail(email)) {
            showAlert("Incorrect input", "Input correct email!");
        } else {
            Ticket ticket = Ticket.builder()
                    .email(email)
                    .name(name)
                    .surname(surname)
                    .way(way)
                    .build();

            try {
                ticket = requestController.orderTicket(ticket);
                AirlineController.ticketToShowInfo = ticket;
                String fxmlFile = "/fxml/TicketInfo.fxml";
                FXMLLoader loader = new FXMLLoader();
                Parent root = loader.load(getClass().getResourceAsStream(fxmlFile));
                Stage stage = new Stage();
                stage.setTitle("Ticket");
                stage.setScene(new Scene(root));
                stage.show();
                Stage oldStage = (Stage) pane.getScene().getWindow();
                oldStage.close();

            } catch (IOException e) {
                showAlert("Something bad happen", "Server didn't accept request, try again");
            }
        }
    }

    private boolean isEmail(String email) {
        return EmailValidator.getInstance().isValid(email);
    }

    private void showAlert(String s, String s2) {
        alert(s, s2, pane);
    }

    static void alert(String s, String s2, AnchorPane pane) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(s);
        alert.setHeaderText(null);
        alert.setContentText(s2);
        if (pane != null) {
            alert.initOwner(pane.getScene().getWindow());
        }
        alert.show();
    }

    private boolean valid(String... vars) {
        for (String str : vars) {
            if (str == null || str.equals("") || str.length() < 3) {
                return false;
            }
        }
        return true;
    }
}
