package com.airline.client.controller;

import com.airline.client.context.AirlineRequestController;
import com.airline.client.domain.City;
import com.airline.client.domain.Ticket;
import com.airline.client.domain.Way;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

public class AirlineController {

    static Ticket ticketToShowInfo;

    public AnchorPane pane;
    public TableView<Way> waysTbl;
    public TableColumn<Way, City> fromCol;
    public TableColumn<Way, LocalDateTime> departureCol;
    public TableColumn<Way, City> toCol;
    public TableColumn<Way, LocalDateTime> arrivalCol;
    public TableColumn<Way, Double> priceCol;
    public ChoiceBox<City> fromBox;
    public ChoiceBox<City> toBox;
    public TextField ticketFld;

    private City from;
    private City to;

    static Way wayToOrder;

    private ObservableList<Way> wayObservableList = FXCollections.observableArrayList();
    private ObservableList<City> fromCities = FXCollections.observableArrayList();
    private ObservableList<City> toCities = FXCollections.observableArrayList();

    private AirlineRequestController requestController = new AirlineRequestController();

    public void initialize() {
        fromCol.setCellValueFactory(new PropertyValueFactory<>("cityDeparture"));
        departureCol.setCellValueFactory(new PropertyValueFactory<>("departure"));
        toCol.setCellValueFactory(new PropertyValueFactory<>("cityArrival"));
        arrivalCol.setCellValueFactory(new PropertyValueFactory<>("arrival"));
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        waysTbl.setItems(wayObservableList);

        fromBox.setItems(fromCities);
        toBox.setItems(toCities);

        ChangeListener<City> changeFromListener = (observable, oldValue, newValue) -> {
            if (newValue != null) {
                from = newValue;
                try {
                    List<City> toList = requestController.getCities(from.getId());
                    toCities.clear();
                    toCities.addAll(toList);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        fromBox.getSelectionModel().selectedItemProperty().addListener(changeFromListener);

        ChangeListener<City> changeToListener = (observable, oldValue, newValue) -> {
            if (newValue != null) {
                to = newValue;
                setTable(from, to);
            }
        };
        toBox.getSelectionModel().selectedItemProperty().addListener(changeToListener);

        initTableMenu();

        setFromBox();
    }

    private void setTable(City from, City to) {
        List<Way> ways = null;
        try {
            ways = requestController.getWays(from.getId(), to.getId());
            wayObservableList.clear();
            wayObservableList.addAll(ways);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initTableMenu() {
        MenuItem order = new MenuItem("Order");
        order.setOnAction(event -> {
            Way way = waysTbl.getSelectionModel().getSelectedItem();
            try {
                openWayOrderWindow(way);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        ContextMenu menu = new ContextMenu();
        menu.getItems().addAll(order);
        waysTbl.setContextMenu(menu);
    }

    private void openWayOrderWindow(Way way) throws IOException {
        wayToOrder = way;
        String fxmlFile = "/fxml/Order.fxml";
        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResourceAsStream(fxmlFile));
        Stage stage = new Stage();
        stage.setTitle("Order");
        stage.setScene(new Scene(root));
        stage.show();
    }

    private void setFromBox() {
        try {
            List<City> cities = requestController.getCities(null);
            fromCities.addAll(cities);
        } catch (IOException e) {
            Platform.runLater(() -> alert("Server didn't response", "Request time out", null));
        }
    }

//    public void searchChair(ActionEvent actionEvent) {
//        String text = chairFld.getText();
//        setFromBox(text);
//    }
//
//    public void searchOrder(ActionEvent actionEvent) {
//        try {
//            Long id = Long.parseLong(orderFld.getText());
//            orderToShowInfo = requestController.orderTicket(id);
//            String fxmlFile = "/fxml/TicketInfo.fxml";
//            FXMLLoader loader = new FXMLLoader();
//            Parent root = loader.load(getClass().getResourceAsStream(fxmlFile));
//            Stage stage = new Stage();
//            stage.setTitle("Order " + orderToShowInfo.getId());
//            stage.setScene(new Scene(root));
//            stage.show();
//        } catch (IOException | NullPointerException e) {
//            alert("Order not found", "Can't find order with such id");
//        } catch (NumberFormatException e) {
//            alert("Not correct input", "Field should contains only numbers");
//        }
//    }

    void alert(String title, String message, AnchorPane pane) {
        OrderController.alert(title, message, pane);
    }

    public void findTicket(ActionEvent actionEvent) throws IOException {
        Long id = 0L;
        try {
            id = Long.valueOf(ticketFld.getText());
        } catch (NumberFormatException e) {
            alert("Invalid input", "Not a number", pane);
            return;
        }
        ticketToShowInfo = requestController.getTicket(id);
        if (ticketToShowInfo != null) {
            String fxmlFile = "/fxml/TicketInfo.fxml";
            FXMLLoader loader = new FXMLLoader();
            Parent root = null;
            try {
                root = loader.load(getClass().getResourceAsStream(fxmlFile));
                Stage stage = new Stage();
                stage.setTitle("Ticket");
                stage.setScene(new Scene(root));
                stage.show();
                Stage oldStage = (Stage) pane.getScene().getWindow();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            alert("Ticket not found", "There are not ticket with such id", pane);
        }
    }
}
