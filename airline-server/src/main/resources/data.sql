insert into city (id, name) values (1, 'Lviv');
insert into city (id, name) values (2, 'Kyiv');
insert into city (id, name) values (3, 'Odesa');
insert into city (id, name) values (4, 'Munchen');
insert into city (id, name) values (5, 'Dublin');
insert into city (id, name) values (6, 'Oslo');
insert into city (id, name) values (7, 'Boston');
insert into city (id, name) values (8, 'New York');

insert into way (id, arrival, departure, city_arrival_id, city_departure_id, price)
VALUES (1, {ts '2012-09-17 18:47:52.69'}, {ts '2012-09-17 18:47:52.69'}, 1, 2, 12);

insert into way (id, arrival, departure, city_arrival_id, city_departure_id, price)
VALUES (2, {ts '2012-09-17 18:47:52.69'}, {ts '2012-09-17 18:47:52.69'}, 2, 1, 12);

insert into way (id, arrival, departure, city_arrival_id, city_departure_id, price)
VALUES (3, {ts '2012-09-17 18:47:52.69'}, {ts '2012-09-17 18:47:52.69'}, 1, 3, 12);

insert into way (id, arrival, departure, city_arrival_id, city_departure_id, price)
VALUES (4, {ts '2012-09-17 18:47:52.69'}, {ts '2012-09-17 18:47:52.69'}, 1, 4, 12);

insert into way (id, arrival, departure, city_arrival_id, city_departure_id, price)
VALUES (5, {ts '2012-09-17 18:47:52.69'}, {ts '2012-09-17 18:47:52.69'}, 3, 2, 12);

insert into way (id, arrival, departure, city_arrival_id, city_departure_id, price)
VALUES (6, {ts '2012-09-17 18:47:52.69'}, {ts '2012-09-17 18:47:52.69'}, 2, 3, 12);

insert into way (id, arrival, departure, city_arrival_id, city_departure_id, price)
VALUES (7, {ts '2012-09-17 18:47:52.69'}, {ts '2012-09-17 18:47:52.69'}, 4, 6, 12);

insert into ticket (id, email, name, surname, way_id)
VALUES (1, 'email@email.em', 'name', 'surname', 1);