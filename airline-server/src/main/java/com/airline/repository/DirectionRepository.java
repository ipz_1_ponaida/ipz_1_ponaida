package com.airline.repository;

import com.airline.model.City;
import com.airline.model.Way;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DirectionRepository extends JpaRepository<Way, Long> {

    List<Way> findByCityDepartureEqualsAndCityArrivalEquals(City departure, City arrival);
    List<Way> findByCityDepartureEquals(City from);
}
