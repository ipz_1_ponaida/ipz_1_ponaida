package com.airline.controller;

import com.airline.model.City;
import com.airline.model.Way;
import com.airline.model.Ticket;
import com.airline.repository.CityRepository;
import com.airline.repository.DirectionRepository;
import com.airline.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class MainController {

    private final TicketRepository ticketRepository;
    private final DirectionRepository directionRepository;
    private final CityRepository cityRepository;

    @Autowired
    public MainController(TicketRepository ticketRepository, DirectionRepository directionRepository, CityRepository cityRepository) {
        this.ticketRepository = ticketRepository;
        this.directionRepository = directionRepository;
        this.cityRepository = cityRepository;
    }

    @RequestMapping(path = "/tickets/{id}", method = RequestMethod.GET)
    public Ticket find(@PathVariable("id") Long id) {
        return ticketRepository.getOne(id);
    }

    @RequestMapping(path = "/tickets", method = RequestMethod.POST)
    public Ticket create(@RequestBody Ticket ticket) {
        return ticketRepository.save(ticket);
    }

    @RequestMapping(path = "/directions", method = RequestMethod.GET)
    public List<Way> getAll(@RequestParam City from, @RequestParam City to) {
        return directionRepository.findByCityDepartureEqualsAndCityArrivalEquals(from, to);
    }

    @RequestMapping(path = "/cities", method = RequestMethod.GET)
    public List<City> findWay(@RequestParam(required = false) Long id) {
        if (id != null) {
            City city = cityRepository.getOne(id);
            return directionRepository.findByCityDepartureEquals(city)
                    .stream()
                    .map(Way::getCityArrival)
                    .collect(Collectors.toList());
        }
        return directionRepository.findAll()
                .stream()
                .map(Way::getCityDeparture)
                .distinct()
                .collect(Collectors.toList());
    }
}
