package com.airline.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class Way {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private City cityDeparture;
    @ManyToOne
    private City cityArrival;
    private LocalDateTime departure;
    private LocalDateTime arrival;
    private Double price;
}